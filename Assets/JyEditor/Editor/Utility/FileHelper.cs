﻿using System.IO;
using System.Collections.Generic;

namespace JyEditor
{
    public class FileHelper : Singleton<FileHelper>
    {
        /// <summary>
        /// 读取目录下的所有文件和文件夹
        /// </summary>
        /// <param name="metaPath"></param>
        /// <param name="files"></param>
        /// <param name="paths"></param>
        public void ReadFile(string metaPath, ref List<string> files, ref List<string> paths)
        {
            if (!Directory.Exists(metaPath))
            {
                Directory.CreateDirectory(metaPath);
            }

            string[] names = Directory.GetFiles(metaPath);
            string[] dirs = Directory.GetDirectories(metaPath);
            foreach (var name in names)
            {
                string ext = Path.GetExtension(name);
                if (ext.Equals(".meta") || ext.Equals(".cs")) continue;
                files.Add(name.Replace('\\', '/'));
            }

            foreach (string dir in dirs)
            {
                paths.Add(dir.Replace('\\', '/'));
                ReadFile(dir, ref files, ref paths);
            }
        }

        /// <summary>
        /// 通过目录和输出路径生成file.txt
        /// </summary>
        /// <param name="path"></param>
        /// <param name="outFilePath"></param>
        public void GenerateFileText(string path, string outFilePath)
        {
            Dictionary<string, JyFileInfo> fileDics = new Dictionary<string, JyFileInfo>();
            List<string> files = new List<string>();
            List<string> paths = new List<string>();
            ReadFile(path, ref files, ref paths);

            int fileCnt = files.Count;
            for (int i = 0; i < fileCnt; i++)
            {
                string filePath = files[i];
                JyFileInfo jyFile = new JyFileInfo(filePath);
                if (!fileDics.ContainsKey(jyFile.FullName))
                    fileDics.Add(jyFile.FullName, jyFile);
            }

            GenerateFileText(fileDics, outFilePath);
        }

        /// <summary>
        /// 创建file.txt文件列表
        /// </summary>
        public void GenerateFileText(Dictionary<string, JyFileInfo> fileDics, string outFilePath)
        {
            if (fileDics == null) return;

            List<string> fileList = new List<string>();

            foreach (var file in fileDics)
            {
                JyFileInfo jyFile = file.Value;

                string temp = string.Format("{0}|{1}|{2}", jyFile.FullName, jyFile.AssetPath, jyFile.Guid);
                fileList.Add(temp);
            }

            string parentPath = Directory.GetParent(outFilePath).ToString();
            if (Directory.Exists(parentPath) && File.Exists(outFilePath))
            {
                File.Delete(outFilePath);
                Directory.Delete(parentPath);
            }

            Directory.CreateDirectory(parentPath);

            FileStream fs = new FileStream(outFilePath, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(fs);

            int cnt = fileList.Count;
            for (int i = 0; i < cnt; i++)
            {
                sw.WriteLine(fileList[i]);
            }
            sw.Close();
            fs.Dispose();
        }

        /// <summary>
        /// 读取file.txt 文件内容
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public Dictionary<string, JyFileInfo> ReadFileText(string filePath)
        {
            string parentPath = Directory.GetParent(filePath).ToString();
            Dictionary<string, JyFileInfo> temp = new Dictionary<string, JyFileInfo>();
            if (Directory.Exists(parentPath) && File.Exists(filePath))
            {
                FileStream fs = new FileStream(filePath, FileMode.Open);
                StreamReader sr = new StreamReader(fs);

                string line;
                while (!string.IsNullOrEmpty(line = sr.ReadLine()))
                {
                    var strs = line.Split('|');
                    if (strs.Length >= 3)
                    {
                        JyFileInfo jf = new JyFileInfo(strs[0], strs[1], strs[2]);
                        temp.Add(strs[0], jf);
                    }
                }
                sr.Close();
                fs.Dispose();
            }

            return temp;
        }

        /// <summary>
        /// 通过目录路径获取所有文件列表
        /// </summary>
        /// <param name="foldPath"></param>
        /// <returns></returns>
        public Dictionary<string, JyFileInfo> ReadFolder(string foldPath)
        {
            Dictionary<string, JyFileInfo> temp = new Dictionary<string, JyFileInfo>();

            List<string> files = new List<string>();
            List<string> paths = new List<string>();
            ReadFile(foldPath, ref files, ref paths);

            int cnt = files.Count;
            for (int i = 0; i < cnt; i++)
            {
                string file = files[i];
                file = file.ToLower();
                JyFileInfo jyFile = new JyFileInfo(file);
                if (!temp.ContainsKey(jyFile.FullName))
                    temp.Add(jyFile.FullName, jyFile);
            }

            return temp;
        }
    }

}