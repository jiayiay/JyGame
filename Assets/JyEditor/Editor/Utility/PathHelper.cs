﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace JyEditor
{

    public class PathHelper : Singleton<PathHelper>
    {
        public static bool IsUseAssetBundleInEditor = true;

        /// <summary>
        /// assetbundle的变体
        /// </summary>
        public const string AssetBundleVariant = "JyAssetBundle";

        /// <summary>
        /// Assetbundle的文件名
        /// </summary>
        public const string AssetBundleFolderName = "AssetBundle";

        /// <summary>
        /// Assets 下资源存放路径
        /// </summary>
        public const string GameResFolderName = "ResourcesCore";

        /// <summary>
        /// file text 名字
        /// </summary>
        public const string FileTextName = "file.txt";

        /// <summary>
        /// 游戏资源路径
        /// </summary>
        public string GameResPath{ get { return _gameResPaht; } }

        /// <summary>
        /// Streaming Assets Path 地址
        /// </summary>
        public string StreamAssetBundlePath { get { return _streamAssetBundlePath; } }

        /// <summary>
        /// 游戏资源AssetBundle打包后存放的路径
        /// </summary>
        public string AssetBundleOutPath
        {
            get
            {
                if (IsUseAssetBundleInEditor)
                {
                    return StreamAssetBundlePath;
                }
                return _outAssetBundlePaht;
            }
        }

        /// <summary>
        /// file.txt 文件路径
        /// </summary>
        public string FileTextPath{ get { return _fileTextPath; } }


        /// <summary>
        /// 私有资源路径(用来打包的地址)
        /// </summary>
        private string _gameResPaht;

        /// <summary>
        /// 打包AssetBundle输出地址
        /// </summary>
        private string _outAssetBundlePaht;

        /// <summary>
        /// file.txt 文件路径
        /// </summary>
        private string _fileTextPath;

        /// <summary>
        /// streaming assets assetbundle
        /// </summary>
        private string _streamAssetBundlePath;

        /// <summary>
        /// 初始化
        /// </summary>
        protected override void Init()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.WindowsEditor:
                    _gameResPaht = string.Format(@"{0}\{1}", Application.dataPath, GameResFolderName);

                    _outAssetBundlePaht = string.Format(@"{0}\{1}", Directory.GetParent(Application.dataPath),
                        AssetBundleFolderName);

                    _streamAssetBundlePath = string.Format(@"{0}\{1}", Application.streamingAssetsPath,
                        AssetBundleFolderName);

                    _fileTextPath = string.Format(@"{0}\{1}", AssetBundleOutPath, FileTextName);
                    break;
                case RuntimePlatform.OSXEditor:
                    _gameResPaht = string.Format("{0}/{1}", Application.dataPath, GameResFolderName);

                    _outAssetBundlePaht = string.Format("{0}/{1}", Directory.GetParent(Application.dataPath),
                        AssetBundleFolderName);

                    _streamAssetBundlePath = string.Format("{0}/{1}", Application.streamingAssetsPath,
                        AssetBundleFolderName);

                    _fileTextPath = string.Format("{0}/{1}", AssetBundleOutPath, FileTextName);
                    break;
            }
        }

    }

}