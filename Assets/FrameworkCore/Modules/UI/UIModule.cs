﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: UI模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// UI模块
    /// </summary>
    public class UIModule : BaseModule
    {
        public UIModule(string name = ModuleName.UI):base(name)
        {
            
        }

        /// <summary>
        /// 初始化本模块
        /// </summary>
        public override void InitGame()
        {
            // 初始化UIManager 并且显示第一个界面
            UIController.CreateSingleton();
        }

        /// <summary>
        /// 更新游戏
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void UpdateGame(float deltaTime)
        {
            base.UpdateGame(deltaTime);

            UIController.Ins.Update(deltaTime);
        }
    }
}
